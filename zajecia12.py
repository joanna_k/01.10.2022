# moduły -> pakiety -> biblioteki ///  framework -

import random

print(random.randint(1,10))

x=[1,2,3,4]
print(len(x))


def add(num1, num2):

    try:
        num3 = int(num2)
        return num1 + num3
    except:
        print("Niepoprawne dane")

n1 = 10
#n2 = input("Wprowadź liczbę:")

#print(add( n1,n2))
#print("Sprawdź czy program dalej działa")


x = 5
y = 0

try:
    10+ '10' # 10+10
except:
    print("Tak nie można")
else:
    print("Możesz dodawać integery!")
finally:
    print("Wykona sie na końcu bloku")



x = 5
y = 0
try:
    z = int(x) / int(y)
except ZeroDivisionError:
    print("Nie można dzielić przez zero!")

finally:
    print("Zakończono działanie programu")

# ---------------------

# Zapisywanie do pliku txt


file = open("pierwszy.txt", "w") #tryb write
file.write("Cześć.")
file.close()

new_content = open("pierwszy.txt", "r")
print(new_content.read())
new_content.close()

with open("pierwszy.txt" , "r") as file:
    print(file.read())

file = open("my_first_file.txt" , "w")
file.write("tekst1")
file.close()

with open("my_first_file.txt", "r") as f:
    print(f.read())

# PEP python enchacement proposal -> PEP8

# klasa - 2 linie odstępu
# funkcja - 1 linia dostępu 

#flake8 A_file.py


# syntethic sugar - 

new_lst = [x*20 for x in range(20)]
print(new_lst)

x = []
for x in range(20):
    new_lst.append(x*20)
print(new_lst)


a=20

x = True if a==20 else False
print(x)

x=1_000_000
print(x)

print(new_lst.__len__())


a = 0
a+=1
print(a)

# Dekoratory - poszerzają funkcję o jakieś działanie

def decorator(func):
    def wrapper():
        print("Drukuje się przed wywołaniem funkcji")
        func()
        print("Drukuje się po wywołaniem funkcji")
    return wrapper

@decorator
def hello():
    print("cześć")

hello()

def hello():
    print("cześć")

hello()

# GIT - system kontroli wersji
